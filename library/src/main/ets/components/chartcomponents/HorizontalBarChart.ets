/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import HorizontalBarChartModel from '../charts/HorizontalBarChartModel';
import { LogUtil } from '../utils/LogUtil';

@Component
export default struct HorizontalBarChartComponent {
  @ObjectLink @Watch("invalidate") model: HorizontalBarChartModel;
  private setting: RenderingContextSettings = new RenderingContextSettings(true);
  private context2D: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.setting);
  @State isPinching: boolean = false;

  invalidate(){
    if (!this.model == undefined) {
      this.model.invalidate();
    }
  }

  aboutToAppear() {
    LogUtil.info('-----------aboutToAppear: enter')
  }

  build() {
    Column() {
      Canvas(this.context2D)
        .onReady(() => {
          if (this.model) {
            this.model.setContext2D(this.context2D);
            this.model.onDraw(this.context2D);
          }
        })
        .onAreaChange((oldArea: Area, newArea: Area) => {
          if (this.model && ((newArea.width !== oldArea.width) || (newArea.height !== oldArea.height))) {
            this.model.onChartSizeChanged(Number(newArea.width), Number(newArea.height), Number(oldArea.width), Number(oldArea.height));
            let jobs = this.model.mJobs;
            let length = jobs.length();
            for (let i = 0; i < length; i++) {
              jobs.get(i).run();
            }
            jobs.clear();
          }
        })
        .onTouch((event) => {
          if(!this.isPinching) {
            this.model?.onTouchEvent(event);
          }
        })
        .priorityGesture(GestureGroup(GestureMode.Exclusive,
          SwipeGesture(),
          TapGesture({ count: 2 })
            .onAction((event?: GestureEvent) => {
              if (event && this.model) {
                this.model.onDoubleTap(false, event);
              }
            }),
          TapGesture({ count: 1 })
            .onAction((event?: GestureEvent) => {
              if (event && this.model) {
                this.model.onSingleTapUp(false, event);
              }
            }),
          LongPressGesture()
            .onAction((event?: GestureEvent) => {
              if (this.model && event) {
                this.model.onLongPress(false, 'Down', event);
              }
            })
            .onActionEnd((event?: GestureEvent) => {
              if (this.model && event) {
                this.model.onLongPress(false, 'Up', event);
              }
            })
            .onActionCancel((event?: GestureEvent) => {
              if (this.model && event) {
                this.model.onLongPress(false, 'Cancel', event);
              }
            }),
          PinchGesture({ fingers: 2 })
            .onActionStart((event?: GestureEvent) => {
              if (this.model && event) {
                this.model.onPinch(false, 'Start', event);
                this.isPinching = true;
              }
            })
            .onActionUpdate((event?: GestureEvent) => {
              if (this.model && event) {
                this.model.onPinch(false, 'Update', event);
                this.isPinching = true;
              }
            })
            .onActionEnd((event?: GestureEvent) => {
              if (this.model && event) {
                this.model.onPinch(false, 'End', event);
                this.isPinching = false;
              }
            })
            .onActionCancel((event?: GestureEvent) => {
              if (this.model && event) {
                this.model.onPinch(false, 'Cancel', event);
                this.isPinching = false;
              }
            })
        ))
    }
  }
}
