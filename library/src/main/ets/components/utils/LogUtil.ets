/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import hilog from '@ohos.hilog'

export class LogUtil {
  public static OFF: number = 0;
  public static FATAL: number = 1
  public static ERROR: number = 2;
  public static WARN: number = 3;
  public static INFO: number = 4;
  public static DEBUG: number = 5;
  public static ALL: number = 6;
  public static mLogLevel:number = LogUtil.ERROR;

  private static domain: number = 0xFF00;
  private static prefix: string = 'MpChart';


  public static debug(format: string, ...args: ESObject): void {
    if (LogUtil.mLogLevel >= LogUtil.DEBUG) {
      hilog.debug(LogUtil.domain, LogUtil.prefix, format, args);
    }
  }

  public static info(format: string, ...args: ESObject): void {
    if (LogUtil.mLogLevel >= LogUtil.INFO) {
      hilog.info(LogUtil.domain, LogUtil.prefix, format, args);
    }
  }

  public static warn(format: string, ...args: ESObject): void {
    if (LogUtil.mLogLevel >= LogUtil.WARN) {
      hilog.warn(LogUtil.domain, LogUtil.prefix, format, args);
    }
  }

  public static error(format: string, ...args: ESObject): void {
    if (LogUtil.mLogLevel >= LogUtil.ERROR) {
      hilog.error(LogUtil.domain, LogUtil.prefix, format, args);
    }
  }

  public static fatal(format: string, ...args: ESObject): void {
    if (LogUtil.mLogLevel >= LogUtil.FATAL) {
      hilog.fatal(LogUtil.domain, LogUtil.prefix, format, args);
    }
  }
}